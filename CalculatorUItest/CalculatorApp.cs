﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Automation;


namespace CalculatorUItest
{
	public class CalculatorApp : IDisposable
	{
		private Process calc;
		//private automation element;
		private AutomationElement window;
		private Stopwatch stopwatch = new Stopwatch();
		private Dictionary<string, AutomationElement> _elements =
			new Dictionary<string, AutomationElement>();


		public CalculatorApp()
		{ }
		public void LaunchAndWaitForInputIdle(string appName, string rootElem,
			int timeoutInMs = 5000)
		{
			//Launch App
			calc = Process.Start(appName);
			stopwatch.Reset();
			stopwatch.Start();
			do
			{
				//Initialize automation element 
				window = AutomationElement.RootElement.FindFirst(
					TreeScope.Children, new PropertyCondition(
                        AutomationElement.NameProperty, rootElem));
				Thread.Sleep(100);
			}
			while (window == null &&
				stopwatch.ElapsedMilliseconds < timeoutInMs);
			if (window == null)
				throw new TimeoutException(appName + " could not be started");
		}
		public void Dispose()
		{
			//Close calculator on completion of test
			PushButton("Close Calculator");
		}
	
		protected void PushButton(string name)
		{
			GetInvokePattern(GetButton(name)).Invoke();
		}

		protected void SelectButton(string name)
		{
			GetSelectionItemPattern(GetButton(name)).Select();
		}

		//control pattern for button control type and list item control type
		protected InvokePattern GetInvokePattern(AutomationElement element)
		{
			return element.GetCurrentPattern(InvokePattern.Pattern) as
				InvokePattern;
		}

		//control pattern for dataitem control type and tab item control type
		protected SelectionItemPattern GetSelectionItemPattern(AutomationElement element)
		{
			return element.GetCurrentPattern(SelectionItemPattern.Pattern) as
				SelectionItemPattern ;
		}

		protected AutomationElement GetButton(string name)
		{
			if (_elements.TryGetValue(name, out AutomationElement elem))
				return elem;
			var result = window.FindFirst(TreeScope.Descendants,
				new PropertyCondition(AutomationElement.NameProperty, name));
			if (result == null)
            {
				result = window.FindFirst(TreeScope.Descendants,
				new PropertyCondition(AutomationElement.ClassNameProperty, name));
			}
            if (result == null)
			{
				throw new ArgumentException(
					"No function button found with name: " + name);
			}
				
			_elements.Add(name, result);
			return result;
		}

		private readonly string[] calculatiorButtons = { "Zero", "One", "Two",
			"Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };

		public void Digit(byte number)
		{
			if ((number < 0) || (number > 9))
				throw new ArgumentException("mumber must be a digit 0-9");
			GetInvokePattern(GetButton(calculatiorButtons[number])).Invoke();
		}
		public void PressNavigation()
		{
			PushButton("Open Navigation");
		}

        public void PressProgrammerMode()
        {
            //Initialize Button and then click on UI
            PushButton("Programmer Calculator");
        }

		public void PressScientificMode()
		{
			//Initialize Button and then click on UI
			PushButton("Scientific Calculator");
		}

		public void PressDateCalculationMode()
		{
			//Initialize Button and then click on UI
			PushButton("Date Calculation Calculator");
		}
		public void PressStandardMode()
		{
			//Initialize Button and then click on UI
			PushButton("Standard Calculator");
		}
		
		public void ClickOnDecimal()
        {
			//Initialize Button and then click on UI
			SelectButton("Decimal 0");
        }
		public void PressAdd()
		{
			//Initialize Button and then click on UI
			PushButton("Plus");
		}
		public void PressMultiply()
		{
			//Initialize Button and then click on UI
			PushButton("Multiply by");
		}
		public void PressSquareButton()
		{
			//Initialize Button and then click on UI
			PushButton("Square");
		}
		public void ClickFromDate()
		{
			//Initialize Button and then click on UI
			PushButton("From");
		}
		public void ChooseDate13()
		{
			//Initialize Button and then click on UI
			SelectButton("13");
		}
		public void ClickToDate()
		{
			//Initialize Button and then click on UI
			PushButton("To");
		}
		public void NavigateToNextPage()
		{
			//Initialize Button and then click on UI
			PushButton("Next");
		}
		public void ChooseDate12()
		{
			//Initialize Button and then click on UI
			SelectButton("12");
		}
		public void PressClearButton()
		{
			//Initialize Button and then click on UI
			PushButton("Clear");
		}
		public void NavigateToHistoryTab()
		{
			//Initialize Button and then click on UI
			SelectButton("History");
		}
		public void ClickOnListView()
		{
			//Initialize Button and then click on UI
			PushButton("ListViewItem");
		}
		public void PressMsButton()
		{
			//Initialize Button and then click on UI
			PushButton("Memory Store");
		}
		public void ClickOnMPlus()
		{
			//Initialize Button and then click on UI
			PushButton("Memory Add");
		}
		public void PressEquals()
		{
			//Initialize Button and then click on UI
			PushButton("Equals");
		}
		public void NavigateToMemoryTab()
		{
			//Initialize Button and then click on UI
			SelectButton("Memory");
		}
		public int ReadHistoryResult()
        {
            //Initialize Button and return result value displayed on the UI
			var resultId = window.FindFirst(TreeScope.Descendants,
			new PropertyCondition(
				AutomationElement.AutomationIdProperty,
				"HistoryItemValue"));
			if (resultId == null)
				throw new Exception("Could not find result box");
			string result = resultId.GetCurrentPropertyValue(
				AutomationElement.NameProperty).ToString();

			return Int32.Parse(Regex.Replace(result, "[^0-9]", String.Empty));
        }
        public int ReadMemoryResult()
        {
            //Initialize Button and return result value displayed on the UI
            var resultId = window.FindFirst(TreeScope.Descendants,
			new PropertyCondition(
				AutomationElement.AutomationIdProperty,
				"MemoryItemValue"));
			if (resultId == null)
				throw new Exception("Could not find result box");
			string result = resultId.GetCurrentPropertyValue(
				AutomationElement.NameProperty).ToString();

			return Int32.Parse(Regex.Replace(result, "[^0-9]", String.Empty));
        }

        public int ReadCurrentResult()
        {
			//Initialize Button and return result value displayed on the UI
			var resultId = window.FindFirst(TreeScope.Descendants,
			new PropertyCondition(
				AutomationElement.AutomationIdProperty,
				"CalculatorResults"));
			if (resultId == null)
				throw new Exception("Could not find result box");
			string result = resultId.GetCurrentPropertyValue(
				AutomationElement.NameProperty).ToString();

			return Int32.Parse(Regex.Replace(result, "[^0-9]", String.Empty));
        }

        public string ReadDifferenceBetweenDateResult()
        {
            //Initialize Button and return result value displayed on the UI
          var resultId = window.FindFirst(TreeScope.Descendants,
			new PropertyCondition(
				AutomationElement.AutomationIdProperty,
				"DateDiffAllUnitsResultLabel"));
			if (resultId == null)
				throw new Exception("Could not find result box");
			string result = resultId.GetCurrentPropertyValue(
				AutomationElement.NameProperty).ToString();
            return result;
        }

        public string ReadResultHex()
        {
			//Initialize Button and return result value displayed on the UI
			var resultId = window.FindFirst(TreeScope.Descendants,
			new PropertyCondition(
				AutomationElement.AutomationIdProperty,
				"hexButton"));
			if (resultId == null)
				throw new Exception("Could not find result box");
            string hexValue = resultId.GetCurrentPropertyValue(
				AutomationElement.NameProperty).ToString();

            return Regex.Replace(hexValue, "[^0-9]", String.Empty);
        }
		public string ReadResultOctol()
		{
			//Initialize Button and return result value displayed on the UI
			var resultId = window.FindFirst(TreeScope.Descendants,
			new PropertyCondition(
				AutomationElement.AutomationIdProperty,
				"octolButton"));
			if (resultId == null)
				throw new Exception("Could not find result box");
			string octolValue = resultId.GetCurrentPropertyValue(
				AutomationElement.NameProperty).ToString();

			return Regex.Replace(octolValue, "[^0-9]", String.Empty);
		}
		public string ReadResultDecimal()
		{
			//Initialize Button and return result value displayed on the UI
			var resultId = window.FindFirst(TreeScope.Descendants,
			new PropertyCondition(
				AutomationElement.AutomationIdProperty,
				"decimalButton"));
			if (resultId == null)
				throw new Exception("Could not find result box");
			string decimalValue = resultId.GetCurrentPropertyValue(
				AutomationElement.NameProperty).ToString();

			return Regex.Replace(decimalValue, "[^0-9]", String.Empty);
		}
		public string ReadResultBinary()
		{
			//Initialize Button and return result value displayed on the UI
			var resultId = window.FindFirst(TreeScope.Descendants,
			new PropertyCondition(
				AutomationElement.AutomationIdProperty,
				"binaryButton"));
			if (resultId == null)
				throw new Exception("Could not find result box");
			string binaryValue = resultId.GetCurrentPropertyValue(
				AutomationElement.NameProperty).ToString();

			return Regex.Replace(binaryValue, "[^0-9]", String.Empty);
		}
	}
}

