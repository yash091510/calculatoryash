﻿Feature: Scenario4
Enter value and verify results in Standard mode in Windows10 Calculator
	
	@myScenario4
	Scenario: Enter data in Standard Mode and Verify results
	Given I launch the Windows 10 Caculator
	And I open Standard mode on the calculator
	And I choose number 20
	And I click on multiply Button
	And I choose 2nd number 30
	When I Click Equal button
	Then Result should be be 600
	Given I clear current result
	And I navigate to History tab
	When I retreive historical data
	Then Retreived data from the History should be visible on Result Section
	Given I enter number 30
	And I click on MS Button
	When I Click on Memory Tab
	Then Number 30 should be visible under Memory tab
	Given I click on number 100
	When I click on M+
	Then Result should be 130
	Then I close the application