﻿using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using TechTalk.SpecFlow.Assist;

namespace CalculatorUItest.TestFunctions
{
 
    [Binding]
    public class CalculatorSteps
    {

        private CalculatorApp calculator;

        [Given(@"I launch the Windows 10 Caculator")]
        public void GivenILaunchTheWindowsCaculator()
        {
            //Launch Calculator Windows 10 application
            calculator = new CalculatorApp();
            calculator.LaunchAndWaitForInputIdle("Calc.exe", "Calculator");
        }


        [Given(@"I open Programmer mode on the calculator")]
        public void GivenIOpenProgrammerModeOnTheCalculator()
        {
            //navigate to programming mode
            Thread.Sleep(1000);
            this.calculator.PressNavigation();
            calculator.PressProgrammerMode();
            
        }


        [Given(@"I choose Decimal and enter numer (.*)")]
        public void GivenIChooseDecimalAndEnterNumer(int p0)
        {
            //Select decimal and Press number 100 on the calculator
            calculator.ClickOnDecimal();
            calculator.Digit(1);
            calculator.Digit(0);
            calculator.Digit(0);
            
        }

        [When(@"I click on Equal Button")]
        public void WhenIClickOnEqualButton()
        {
            //Press Equal button
            calculator.PressEquals();
        }

        [Then(@"Value of Hex, Bin, Dec and Oct values are displayed on the screen")]
        public void ThenValueOfHexBinDecAndOctValuesAreDisplayedOnTheScreen(Table resultTable)
        {
            //Fetch Data displayed on the Calculator to verify
            var hexValue = calculator.ReadResultHex();
            var octolValue = calculator.ReadResultOctol();
            var decimalValue = calculator.ReadResultDecimal();
            var binaryValue = calculator.ReadResultBinary();


           // Fetvh Data from the Test Scenario to verify
            var data = resultTable.CreateSet<ResultData>();

            foreach (ResultData result in data)
            {
                if (result.Type == "Hex")
                {
                    Assert.IsTrue(result.Value == hexValue);

                }
                else if (result.Type == "Oct")
                {
                    Assert.IsTrue(result.Value == octolValue);
                }
                else if (result.Type == "Dec")
                {
                    Assert.IsTrue(result.Value == decimalValue);
                }
                else if (result.Type == "Bin")
                {
                    Assert.IsTrue(result.Value == binaryValue);
                }
                else
                {
                    Assert.Fail("Table type mismatch");
                }

            }

        }


        [Given(@"I open Scientific mode on the calculator")]
        public void GivenIOpenScientificModeOnTheCalculator()
        {
            //Navigate to sceintific mode
            Thread.Sleep(1000);
            calculator.PressNavigation();
            calculator.PressScientificMode();
        }

        [Given(@"I choose number as (.*)")]
        public void GivenIChooseNumberAs(int number)
        {
            //Press 8 button on calculator
            calculator.Digit(8);
        }

        [Given(@"I Click on Button to Calculate square of the number")]
        public void GivenIClickOnButtonToCalculateSquareOfTheNumber()
        {
            //click on square of the number button
            calculator.PressSquareButton();
        }

        [When(@"I press Equal Button")]
        public void WhenIPressEqualButton()
        {
            //Press equal button to display result on calculator
            calculator.PressEquals();
        }


        [Then(@"the result should be (.*)")]
        public void ThenTheResultShouldBe(int value)
        {
            //Fetch data displayed on the calculator to verify
            int result = calculator.ReadCurrentResult(); 
            Assert.IsTrue(result == value);
        }

        [Given(@"I open Date Calculation mode on the calculator")]
        public void GivenIOpenDateCalculationModeOnTheCalculator()
        {
            //navigate to Date calculation mode on calculator
            Thread.Sleep(1000);
            calculator.PressNavigation();
            calculator.PressDateCalculationMode();
        }

        [Given(@"I choose From-Date 13th Nov 21")]
        public void GivenIChooseFrom_DateThNov()
        {
            //Select from date
            calculator.ClickFromDate();
            Thread.Sleep(1000);
            calculator.ChooseDate13();
            Thread.Sleep(1000);
        }

        [Given(@"I choose To-Date 12th Dec 21")]
        public void GivenIChooseTo_DateThDec()
        {
            //select To-date
            calculator.ClickToDate();
            Thread.Sleep(1000);
            calculator.NavigateToNextPage();
            Thread.Sleep(1000);
            calculator.ChooseDate12();
            Thread.Sleep(1000);
        }

        [Then(@"Difference between two dates should be be ""(.*)""")]
        public void ThenDifferenceBetweenTwoDatesShouldBeBe(string value)
        {
            //Fetch Data displayed on the calculator to verify
            string result = calculator.ReadDifferenceBetweenDateResult().Replace("Difference ", "");
            Assert.IsTrue(result == value);
        }

        [Given(@"I open Standard mode on the calculator")]
        public void GivenIOpenStandardModeOnTheCalculator()
        {
            //Navigate to standard mode on the calculator
            Thread.Sleep(1000);
            calculator.PressNavigation();
            calculator.PressStandardMode();
        }

        [Given(@"I choose number (.*)")]
        public void GivenIChooseNumber(int firstNumber)
        {
            //Select number 20 on the calculator
            calculator.Digit(2);
            calculator.Digit(0);
        }

        [Given(@"I click on multiply Button")]
        public void GivenIClickOnMultiplyButton()
        {
            //Click on multiply button on calculator
            calculator.PressMultiply();
        }
        [Given(@"I choose 2nd number (.*)")]
        public void GivenIChooseNdNumber(int secondNumber)
        {
            //slect number 30 on calculator
            calculator.Digit(3);
            calculator.Digit(0);
        }


        [When(@"I Click Equal button")]
        public void WhenIClickEqualButton()
        {
            //Press equal button to display result on calculator
            calculator.PressEquals();
            Thread.Sleep(1000);
        }

        [Then(@"Result should be be (.*)")]
        public void ThenResultShouldBeBe(int value)
        {
            //Fetch data displayed on the calculator to verify
            int result = calculator.ReadCurrentResult();
            Assert.IsTrue(result == value);

        }

        [Given(@"I clear current result")]
        public void GivenIClearCurrentResult()
        {
            //click on clear button
          calculator.PressClearButton();
            Thread.Sleep(500);

        }

        [Given(@"I navigate to History tab")]
        public void GivenINavigateToHistoryTab()
        {
            //Click on history tab
           calculator.NavigateToHistoryTab();
        }

        [When(@"I retreive historical data")]
        public void WhenIRetreiveHistoricalData()
        {
            //click on History item to display on Result 
           calculator.ClickOnListView();
            Thread.Sleep(1000);
        }

        [Then(@"Retreived data from the History should be visible on Result Section")]
        public void ThenRetreivedDataFromTheHistoryShouldBeVisibleOnResultSection()
        {
            //fetch data from the calculator to verify
            int resultData = calculator.ReadCurrentResult() ;
            int historyData = calculator.ReadHistoryResult() ;
            Assert.IsTrue(resultData == historyData);
        }

        [Given(@"I enter number (.*)")]
        public void GivenIEnterNumber(int p0)
        {
            //enter number 30 on the calculator
            calculator.PressClearButton();
            calculator.Digit(3);
            calculator.Digit(0);

        }

        [Given(@"I click on MS Button")]
        public void GivenIClickOnMSButton()
        {
            //click on ms buttom
            calculator.PressMsButton();
        }

        [When(@"I Click on Memory Tab")]
        public void WhenIClickOnMemoryTab()
        {
            //navigate to memory tab
           calculator.NavigateToMemoryTab();
            Thread.Sleep(1000);
        }

        [Then(@"Number (.*) should be visible under Memory tab")]
        public void ThenNumberShouldBeVisibleUnderMemoryTab(int value)
        {
            //fetch data displayed on memory tab to verify
            int memoryData = calculator.ReadMemoryResult(); ;
            Assert.IsTrue(memoryData == value);

        }

        [Given(@"I click on number 100")]
        public void GivenIClickOnNumber()
        {
            //enter number 100 on the calculator
            calculator.Digit(1);
            calculator.Digit(0);
            calculator.Digit(0);

        }

        [When(@"I click on M\+")]
        public void WhenIClickOnM()
        {
            //Click on M+
            calculator.ClickOnMPlus();
            Thread.Sleep(2000);
        }

        [Then(@"Result should be (.*)")]
        public void ThenResultShouldBe(int value)
        {
            //Fetch data from the calulator to verify
            int result = calculator.ReadMemoryResult();
            Assert.IsTrue(result == value);
        }


        [Then(@"I close the application")]
        public void ThenICloseTheApplication()
        {
            //Close the calculator
            Thread.Sleep(1000);
            calculator.Dispose();
        }
    }
}
