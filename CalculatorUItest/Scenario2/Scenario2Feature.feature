﻿Feature: Scenario2
Enter value and verify results in scientific mode in Windows10 Calculator
@myScenario2
	Scenario: Enter a value and verify results in scientific mode
    Given I launch the Windows 10 Caculator 
	And I open Scientific mode on the calculator
	And I choose number as 8
	And I Click on Button to Calculate square of the number
	When I press Equal Button
	Then the result should be 64
	Then I close the application