﻿Feature: Scenario3
Enter value and verify results in Date calculation mode in Windows10 Calculator
@myScenario3
	Scenario: Enter a Date and Verify Day and Week in Date Calculation Mode
	Given I launch the Windows 10 Caculator
	And I open Date Calculation mode on the calculator
	And I choose From-Date 13th Nov 21
	And I choose To-Date 12th Dec 21
	Then Difference between two dates should be be "4 weeks, 1 day"
	Then I close the application