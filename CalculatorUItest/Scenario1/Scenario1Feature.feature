﻿Feature: Scenario1
Enter value and verify results in Programmer mode in Windows10 Calculator

	@myScenario1
	Scenario: Enter a value and verify Hex,Dec,Oct,Bin values in Programming Mode
	Given I launch the Windows 10 Caculator
	And I open Programmer mode on the calculator
	And I choose Decimal and enter numer 100
	When I click on Equal Button
	Then Value of Hex, Bin, Dec and Oct values are displayed on the screen
	|Type|Value|
	| Hex | 64 |
	| Oct | 144 |
	| Dec | 100 |
	| Bin | 01100100 |
	Then I close the application
